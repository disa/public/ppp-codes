/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.index;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;
import messif.algorithms.AlgorithmMethodException;
import pppcodes.PPPCodeLeafCellFIRSTTest;

public class TestCompression {

    private static final Logger LOG = Logger.getLogger(TestCompression.class.toString());

    public static byte[] compress(byte[] data) throws IOException {
        Deflater deflater = new Deflater(Deflater.BEST_COMPRESSION);
        deflater.setInput(data);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);

        deflater.finish();
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer); // returns the generated code... index  
            outputStream.write(buffer, 0, count);
        }
        outputStream.close();
        byte[] output = outputStream.toByteArray();

        deflater.end();

        System.out.println("Original: " + data.length + " bytes");
        System.out.println("Compressed: " + output.length + " bytes");
        return output;
    }

    public static byte[] decompress(byte[] data) throws IOException, DataFormatException {
        Inflater inflater = new Inflater();
        inflater.setInput(data);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        while (!inflater.finished()) {
            int count = inflater.inflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        outputStream.close();
        byte[] output = outputStream.toByteArray();

        inflater.end();

        System.out.println("Original: " + data.length);
        System.out.println("Uncompressed: " + output.length);
        return output;
    }

    public static void main(String[] args) {
        try {
            PPPCodeLeafCellFIRSTTest leafTest = new PPPCodeLeafCellFIRSTTest();
            leafTest.setUp();
            leafTest.insertObjects(1000);
            byte[] data = Arrays.copyOf(leafTest.leaf.data, leafTest.leaf.writeBufferBits.position());
            byte[] compressed = compress(data);
            
        } catch (Exception ex) {
            Logger.getLogger(TestCompression.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
