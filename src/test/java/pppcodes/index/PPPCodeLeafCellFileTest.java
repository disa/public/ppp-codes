/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pppcodes.index;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import messif.algorithms.AlgorithmMethodException;
import messif.buckets.BucketStorageException;
import messif.buckets.storage.impl.DiskStorage;
import messif.buckets.storage.impl.DiskStorageFillGaps;
import org.jmock.Expectations;
import static org.jmock.Expectations.returnValue;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Test;
import org.junit.runner.RunWith;
import pppcodes.PPPCodeIndexFile;
import pppcodes.index.persistent.PPPCodeInternalCellFile;
import pppcodes.index.persistent.PPPCodeLeafCellFile;

/**
 *
 * @author david
 */
@RunWith(JMock.class)
public class PPPCodeLeafCellFileTest {
    
    public PPPCodeLeafCellFileTest() throws InstantiationException, IOException {
        params.put("file", File.createTempFile("test-storage", ".tmp"));
        params.put("bufferSize", 4096);
        System.out.println("creating file storage: " + params.get("file"));
        this.leafStorage = DiskStorageFillGaps.create(PPPCodeLeafCellFile.PPPCodeLeafData.class, params);
    }

    private Mockery context = new JUnit4Mockery() {
            {
                setImposteriser(ClassImposteriser.INSTANCE);
            }
        };    
    private final PPPCodeIndexFile mIndexMock = context.mock(PPPCodeIndexFile.class);

    final Map<String, Object> params = new HashMap<>();
    final DiskStorage<PPPCodeLeafCellFile.PPPCodeLeafData> leafStorage;
    final PPPCodeInternalCellFile parent = context.mock(PPPCodeInternalCellFile.class);

    private PPPCodeLeafCellFile initData() throws AlgorithmMethodException, IOException, InstantiationException {        
        context.checking(new Expectations() {
            {
                allowing(parent).getLevel();
                will(returnValue((short) 2));
                allowing(parent).getChildPPP(null);
                will(returnValue(new short [] {(short) 1, (short) 2, (short) 3}));
                allowing(mIndexMock).getMaxLevel();
                will(returnValue((short) 4));
                allowing(mIndexMock).getNumberOfPivots();
                will(returnValue((short) 100));
                allowing(mIndexMock).getMaxObjectNumber();
                will(returnValue((long) 0));
                allowing(mIndexMock).getPPPCodeReadWriter();
                will(returnValue(new PPPCodeReadWriter(100, (short) 6, 0L)));
                allowing(mIndexMock).getLeafStorage();
                will(returnValue(leafStorage));
            }
        });
        
        
        PPPCodeLeafCellFile instance = new PPPCodeLeafCellFile(mIndexMock, parent);
        // insert data
        PPPCodeObject pppCodeObject = new PPPCodeObject(new int [] {312, 12, 3, 4, 1, 90}, new short [] {(short) 1, (short) 2, (short) 3, (short) 4});
        instance.insertObjects(Collections.singletonList(pppCodeObject));
        System.out.println("1st object: " + Arrays.toString(pppCodeObject.getLocators()));
//        pppCodeObject = new PPPCodeObject(new int [] {12, 212, 13, 44, 144, 0}, new short [] {(short) 1, (short) 2, (short) 3, (short) 5});
        pppCodeObject = new PPPCodeObject(new int [] {144}, new short [] {(short) 1, (short) 2, (short) 3, (short) 5});
        instance.insertObjects(Collections.singletonList(pppCodeObject));
        System.out.println("2nd object: " + Arrays.toString(pppCodeObject.getLocators()));

        return instance;
    }
    
    private void printContent(PPPCodeLeafCellFile instance) {
        System.out.println("current data:");
        for (Iterator<PPPCodeObject> iterator = instance.getAllObjects(); iterator.hasNext();) {
            System.out.println(Arrays.toString(iterator.next().getLocators()));
        }
    }
   
    @Test
    public void testDeleteAndInsert() throws AlgorithmMethodException, IOException, InstantiationException, BucketStorageException {
        System.out.println("testDeleteAndInsert");
        for (int i = 0; i < 1; i++) {
            PPPCodeLeafCellFile instance = initData();
            printContent(instance);

            Collection<PPPCodeObject> objects = new ArrayList<>();
//            objects.add(new PPPCodeObject(new int [] {144}, new short [] {(short) 1, (short) 2, (short) 3, (short) 5}));
//            objects.add(new PPPCodeObject(new int [] {12}, new short [] {(short) 1, (short) 2, (short) 3, (short) 4}));
//            objects.add(new PPPCodeObject(new int [] {12}, new short [] {(short) 1, (short) 2, (short) 3, (short) 4}));
            Random random = new Random();
            for (int j = 0; j < 50; j++) {
                objects.add(new PPPCodeObject(new int [] {random.nextInt(10000)}, new short [] {(short) 1, (short) 2, (short) 3, (short) random.nextInt(6)}));
            }
            
            for (PPPCodeObject obj : objects) {
                // remove one object
//                System.out.println("removing ID " + Arrays.toString(obj.getLocators()));
                instance.deleteObjects(Collections.singletonList(obj), 0, true);
//                printContent(instance);

                //System.out.println("consolidating");
                //instance.consolidateData(null);
//                printContent(instance);

//                System.out.println("inserting ID " + Arrays.toString(obj.getLocators()));
                instance.insertObjects(Collections.singletonList(obj));
//                printContent(instance);

                //System.out.println("consolidating");
//                instance.consolidateData(null);
//                instance.reloadDataTest();
                //printContent(instance);
                //System.out.println("\n");
            }
            System.out.println("consolidating");
            instance.consolidateData(null);
            instance.reloadDataTest();
            printContent(instance);
            System.out.println("\n");
            
        }        
    }
        
    
//    /**
//     * Test of consolidateData method, of class PPPCodeLeafCell.
//     */
//    @Test
//    public void testConsolidateData() {
//        System.out.println("consolidateData");
//        byte[] temporary = null;
//        PPPCodeLeafCell instance = null;
//        instance.consolidateData(temporary);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    
}
