/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.ids;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class DirectStringIntConvertorTest {
    
    public DirectStringIntConvertorTest() {
    }

    /**
     * Test of getIntLocator method, of class DirectStringIntConvertor.
     */
    @Test
    public void testGetIntLocator() {
        for (int i = 0; i < 11; i++) {
            System.out.println("getIntLocator int size " + i);
            String locator = "0000123";
            DirectStringIntConvertor instance = new DirectStringIntConvertor();
            int expResult = 123;
            int result = instance.getIntLocator(locator);
            assertEquals(expResult, result);
        }
    }

    /**
     * Test of getStringLocator method, of class DirectStringIntConvertor.
     */
    @Test
    public void testGetStringLocator() {
        for (int i = 0; i < 11; i++) {
            System.out.println("getStringLocator, int size " + i);
            int id = 123;
            DirectStringIntConvertor instance = new DirectStringIntConvertor(i);
            String expResult = "00000000000000000".substring(0,Math.max(0,i-3)) + "123";
            String result = instance.getStringLocator(id);
            System.out.println("output:" + result);
            assertEquals(expResult, result);
        }
    }
    
}
