
package pppcodes.processors;

import messif.objects.AbstractObject;
import messif.objects.NoDataObject;
import messif.operations.query.GetRandomObjectsQueryOperation;
import mindex.navigation.VoronoiInternalCell;
import mindex.processors.GetRandomObjectsNavigationProcessor;
import pppcodes.ids.LocatorStringIntConvertor;
import pppcodes.index.PPPCodeObject;

/**
 * Traverses a single PPP-Tree for random objects and returns a list of {@link NoDataObject} instances with the string locators.
 * @author xnovak8
 */
public class GetRandomNoDataObjectsNavigationProcessor extends GetRandomObjectsNavigationProcessor {

    /** The int-string ID translator. It can be null, than it is not used and the integers are transformed to strings */
    protected final LocatorStringIntConvertor locatorTranslator;

    /**
     * Creates new processor to get random objects from given PPP-Tree.
     * @param operation encapsulating operation
     * @param voronoiTreeRoot root of the PPP-Tree to traverse for random objects
     * @param locTranslator translator of internal integer IDs to String locators
     */
    public GetRandomNoDataObjectsNavigationProcessor(GetRandomObjectsQueryOperation operation, VoronoiInternalCell<PPPCodeObject> voronoiTreeRoot, LocatorStringIntConvertor locTranslator) {
        super(operation, voronoiTreeRoot);
        this.locatorTranslator = locTranslator;
    }    

    @Override
    protected void addToAnswer(AbstractObject nextRandom) throws IllegalArgumentException {
        if (nextRandom instanceof PPPCodeObject) {
            int [] ids = ((PPPCodeObject) nextRandom).getLocators();
            super.addToAnswer(new NoDataObject(locatorTranslator.getStringLocator(ids[random.nextInt(ids.length)])));
        } else {
            super.addToAnswer(nextRandom);
        }
    }
}
