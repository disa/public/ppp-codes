/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.processors;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import messif.operations.AbstractOperation;
import messif.operations.GetCandidateSetOperation;
import messif.operations.RankingSingleQueryOperation;
import pppcodes.algorithms.PPPCodeSingleAlgorithm;
import pppcodes.ids.LocatorStringIntConvertor;

/**
 * This navigation process takes {@link GetCandidateSetOperation} and puts the top candidate objects to this operation.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class ApproxNavProcessorCandSet extends ApproxNavProcessorNorefine {    
    
    /** Output of this processor - queue of string object locators. */
    protected final BlockingQueue<String> objectLocatorQueue;
    
    /** The operation to return the candidate set or required size. */
    protected final GetCandidateSetOperation getCandOperation;
    
    /**
     * Creates new approximate navigation processor for given operation list of individual PPP-Code indexes. 
     *  A default percentile 0.5 (median) is used for merging individual sub-indexes priority queues.
     * @param operation approximate kNN operation (should be the same for approximate range)
     * @param indexes list of sub-indexes
     * @param locTranslator object that can translate the object IDs to locators after processing
     */
    public ApproxNavProcessorCandSet(GetCandidateSetOperation operation, List<PPPCodeSingleAlgorithm> indexes, LocatorStringIntConvertor locTranslator) {
        super(operation.getEncapsulatedOperation(), indexes, locTranslator, operation.getCandidateSetSize()); 
        this.objectLocatorQueue = operation.getCandidateSetLocators();
        this.getCandOperation = operation;
    }    

    @Override
    public AbstractOperation getOperation() {
        return getCandOperation;
    }
    
    @Override
    protected boolean continueMerging() {
        return (returnedObjects < candidateSetSize);
    }    
    
    @Override
    protected void addIDToCandidateSet(int id) {
        returnedObjects ++;
        try {
            objectLocatorQueue.add(locatorTranslator.getStringLocator(id));
        } catch (IllegalStateException ignore) { }
    }
    
    @Override
    public void close() {
        super.close();
        if (! getCandOperation.isFinished()) {
            getCandOperation.endOperation();
        }
    }
    
}
