/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.processors;

import java.util.List;
import messif.objects.NoDataObject;
import messif.operations.RankingSingleQueryOperation;
import pppcodes.algorithms.PPPCodeSingleAlgorithm;
import pppcodes.ids.LocatorStringIntConvertor;

/**
 * This navigation process takes KNNQueryOperation and stores the first non-refined candidate objects to the operation answer.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class ApproxNavProcessorNorefine extends ApproxNavProcessorPPPCodes {
    
    /** Number of (candidate) objects to return. */
    protected int candidateSetSize;    

    /** Number of objects already returned */
    protected volatile int returnedObjects = 0;
    
    /** The int-string ID translator. It can be null, than it is not used and the integers are transformed to strings */
    protected final LocatorStringIntConvertor locatorTranslator;
    
    /**
     * Creates new approximate navigation processor for given operation list of individual PPP-Code indexes. 
     *  A default percentile 0.5 (median) is used for merging individual sub-indexes priority queues.
     * @param operation approximate kNN operation (should be the same for approximate range)
     * @param indexes list of sub-indexes
     * @param locTranslator object that can translate the object IDs to locators after processing
     * @param candidateSetSize
     */
    public ApproxNavProcessorNorefine(RankingSingleQueryOperation operation, List<PPPCodeSingleAlgorithm> indexes, LocatorStringIntConvertor locTranslator, int candidateSetSize) {
        super(operation, indexes);//, new ArrayDeque<Integer>((int) (1.2f * candidateSetSize)));
        this.candidateSetSize = candidateSetSize;
                      
        this.locatorTranslator = locTranslator;
    }    
        
    @Override
    protected boolean continueMerging() {
        updateStatistics(returnedObjects);
        
        return (returnedObjects < candidateSetSize);
    }

    @Override
    protected void addIDToCandidateSet(int id) {
        returnedObjects ++;
        originalOperation.addToAnswer(new NoDataObject(locatorTranslator.getStringLocator(id)), (float) originalOperation.getAnswerCount(), null);
    }    
    
    @Override
    public void close() {
        super.close();
        if (! originalOperation.isFinished()) {
            originalOperation.endOperation();
        }
    }
    
}
