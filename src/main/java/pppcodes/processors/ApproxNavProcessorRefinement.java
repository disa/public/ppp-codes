/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.processors;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import messif.algorithms.AlgorithmMethodException;
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectIterator;
import messif.operations.AbstractOperation;
import messif.operations.Approximate;
import messif.operations.RankingSingleQueryOperation;
import messif.operations.query.ApproxKNNQueryOperation;
import messif.statistics.OperationStatistics;
import mindex.MetricIndexes;
import pppcodes.algorithms.PPPCodeSingleAlgorithm;
import pppcodes.ids.IDObjectRAStorage;

/**
 * This navigation process reads the continuous candidate set and refines it using internal id-object storage.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class ApproxNavProcessorRefinement extends ApproxNavProcessorPPPCodes {

    /** Size of the candidate set queue (buffer). */
    public final static int DEFAULT_NR_OF_ACCESSED = 1000;

    /** Maximum length of the internal buffer queues (if full, then the producers are stopped). */
    protected final static int MAX_QUEUE_LENGTH = 512;

    protected final static int N_REFINEMENT_THREADS = 4;    
    
    /** Random access storage of objects hash-indexed by their integer IDs. */
    protected final IDObjectRAStorage<? extends LocalAbstractObject> idObjectStorage;
    
    /** Maximal number of data objects to be processed (refined) by this processor. */
    private final int maxNumberOfAccessed;
    
    /** Counter of data objects processed (refined) by this processor. */
    //protected volatile int candSizeProcessed = 0;
    protected AtomicInteger candSizeProcessed = new AtomicInteger();

    /** FIFO structure of candidate objects to be refined by this processor. */
    protected final BlockingQueue<Integer> candidateIDQueue;

    /** FIFO structure of the candidate objects already extracted from the {@link #idObjectStorage} and waiting for d(q,x). */
    protected final BlockingQueue<LocalAbstractObject> resolvedObjects;

    /** Temporary queue of iterators over objects whose resolving was started */
    protected final BlockingQueue<Iterator<? extends LocalAbstractObject>> currentlyReadObjects;
    
    /** Number of objects already resolved or being resolved at the moment. */
    protected volatile int nObjectsResolved = 0;
    
    protected AtomicBoolean objectResolvementStarted = new AtomicBoolean(false);
    protected AtomicBoolean readingResolvedObjsStarted = new AtomicBoolean(false);
    protected AtomicInteger refinementToStart = new AtomicInteger(N_REFINEMENT_THREADS);
    protected AtomicInteger refinementToEnd = new AtomicInteger(0);
    
    /** Statistics */    
    protected long resolutionTime = 0L;
    
    /**
     * Creates new approximate navigation processor for given operation list of individual PPP-Code indexes. 
     *  A default percentile 0.5 (median) is used for merging individual sub-indexes priority queues.
     * @param idObjectStorage locator-object storage to read the objects to be refined
     * @param operation approximate kNN operation (should be the same for approximate range)
     * @param indexes list of sub-indexes
     */
    public ApproxNavProcessorRefinement(IDObjectRAStorage idObjectStorage, ApproxKNNQueryOperation operation, List<PPPCodeSingleAlgorithm> indexes) {
        super(operation, indexes);
        this.candidateIDQueue = new ArrayBlockingQueue<>(MAX_QUEUE_LENGTH);
        this.resolvedObjects = new ArrayBlockingQueue<>(MAX_QUEUE_LENGTH);
        this.currentlyReadObjects = new ArrayBlockingQueue<>(16);
        if (operation.getLocalSearchType() == Approximate.LocalSearchType.ABS_OBJ_COUNT) {
            this.maxNumberOfAccessed = operation.getLocalSearchParam();
        } else {
            this.maxNumberOfAccessed = DEFAULT_NR_OF_ACCESSED;
        }
        this.idObjectStorage = idObjectStorage;
        
        // create the object filter, if required
        idObjectStorage.preprocessObject(operation.getQueryObject());
    }

    /**
     * Read next batch of objects from the FIFO and read the ID-specified objects from the RA storage.
     */
    protected void resolveObjectsFromIDs() {
        List<Integer> currentCandSet = new ArrayList<>();
        while (continuteObjectResolving()) {
            // read the so far processed candidate set into a temporary list
            currentCandSet.clear();
            try {
                if (candidateIDQueue.isEmpty() && continuteObjectResolving()) {
                    // this is a BLOCKING operation
                    currentCandSet.add(candidateIDQueue.take());
                }
                while (!candidateIDQueue.isEmpty()) {
                    currentCandSet.add(candidateIDQueue.poll());
                }
                if (! continuteObjectResolving()) {
                    return;
                }
                
                // refine the candidate set and create final answer
                currentlyReadObjects.put(idObjectStorage.readObjects(currentCandSet));
                nObjectsResolved += currentCandSet.size();
            } catch (InterruptedException ex) {
                MetricIndexes.logger.log(Level.WARNING, "thread interrupted while resolving objects from IDs: {0}", ex);
            }
        }
    }
    
    /**
     * This method runs in a separate thread and it reads resolved objects from the temporary queue {@link #currentlyReadObjects}
     *  and puts them to {@link #resolvedObjects}.
     */
    protected void waitForReadObjects() {
        while (continuteObjectResolving() || ! currentlyReadObjects.isEmpty()) {            
            try {
                Iterator<? extends LocalAbstractObject> readObjects = currentlyReadObjects.take();
                if (refinementToEnd.get() <= 0 && refinementToStart.get() <= 0 ) {
                    return;
                }
                long startTime = System.currentTimeMillis();
                while (readObjects.hasNext()) {
                    LocalAbstractObject insertedObj = readObjects.next();
                    if (! resolvedObjects.offer(insertedObj) && (refinementToEnd.get() > 0 || refinementToStart.get() > 0)) {
                        resolvedObjects.put(insertedObj);
                    }
                }
                resolutionTime += System.currentTimeMillis() - startTime;                
            } catch (InterruptedException ex) {
                MetricIndexes.logger.log(Level.WARNING, "thread interrupted while waiting for objects resolved from IDs: {0}", ex);
            }
        }
    }
    
    /**
     * Read next batch of objects from the FIFO and refine them.
     */
    protected void refineObjectsByDC() {
        originalOperation.evaluate(new AbstractObjectIterator<LocalAbstractObject>() {
            private LocalAbstractObject currentObject;
            /** Flag for remembering if next() has been called. */
            protected int hasNext = -1;
            
            @Override
            public LocalAbstractObject getCurrentObject() throws NoSuchElementException {
                if (currentObject == null) {
                    throw new NoSuchElementException("method next() was not called yet");
                }
                return currentObject;
            }

            @Override
            public boolean hasNext() {
                if (! continuteDCRefinement()) {
                    hasNext = 0;
                    return false;
                }

                if (hasNext == -1) {
                    try {
                        // this is a blocking operation
                        do {
                            currentObject = resolvedObjects.poll(1, TimeUnit.MICROSECONDS);
                        } while (currentObject == null && continuteDCRefinement());
                        hasNext = (currentObject == null) ? 0 : 1;
                    } catch (InterruptedException ex) {
                        MetricIndexes.logger.log(Level.WARNING, "thread interrupted while waiting for ''resolvedObjects'': {0}", ex);
                        hasNext = 0;
                    }                    
                }
                return hasNext == 1;
            }

            @Override
            public LocalAbstractObject next() {
                if (hasNext != 1)
                    throw new NoSuchElementException("There are no more objects");
                hasNext = -1;
                candSizeProcessed.incrementAndGet();
                return currentObject;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported."); 
            }
        });
        refinementToEnd.decrementAndGet();
    }
    
    protected boolean continuteDCRefinement() {
        return candSizeProcessed.get() < maxNumberOfAccessed;
    }

    /**
     * Method that says if the objects should still be resolved from their IDs. 
     *  Check, if there are still some threads reading the resolved objects.
     * @return 
     */
    protected boolean continuteObjectResolving() {
        return (nObjectsResolved < maxNumberOfAccessed);
    }
    
    @Override
    protected boolean continueMerging() {
        // create the statistics
        int objectsMerged = nObjectsResolved + candidateIDQueue.size();
        updateStatistics(objectsMerged);
        return objectsMerged < maxNumberOfAccessed;
    }

    @Override
    protected void addIDToCandidateSet(int id) {
        try {
            if (! candidateIDQueue.offer(id)) {
                if (continueMerging()) {
                    // blocking operation
                    candidateIDQueue.put(id);
                }
            }
        } catch (InterruptedException ex) { }
    }
    
    @Override
    public Callable<AbstractOperation> processStepAsynchronously() throws InterruptedException {
        Callable<AbstractOperation> nextStep = super.processStepAsynchronously();
        if (nextStep != null) {
            return nextStep;
        }
        
        if (! objectResolvementStarted.getAndSet(true)) {
            return new Callable<AbstractOperation>() {
                @Override
                public RankingSingleQueryOperation call() throws InterruptedException, CloneNotSupportedException, AlgorithmMethodException {
                    resolveObjectsFromIDs();
                    candidateIDQueue.clear();
                    return originalOperation;
                }
            };
        }

        if (! readingResolvedObjsStarted.getAndSet(true)) {
            return new Callable<AbstractOperation>() {
                @Override
                public RankingSingleQueryOperation call() throws InterruptedException, CloneNotSupportedException, AlgorithmMethodException {
                    waitForReadObjects();
                    currentlyReadObjects.clear();
                    return originalOperation;
                }
            };
        }
        
        if (refinementToStart.getAndDecrement() > 0) {
            refinementToEnd.incrementAndGet();
            return new Callable<AbstractOperation>() {
                @Override
                public RankingSingleQueryOperation call() throws InterruptedException, CloneNotSupportedException, AlgorithmMethodException {
                    refineObjectsByDC();
                    if (refinementToEnd.get() <= 0 && refinementToStart.get() <= 0) {
                        resolvedObjects.clear();
                    }
                    return originalOperation;
                }
            };            
        }
        return null;
    }

    @Override
    public boolean processStep() throws InterruptedException, AlgorithmMethodException, CloneNotSupportedException {
        throw new AlgorithmMethodException("the " + this.getClass() + " class is ready only for asynchronous processing.");
    }

    @Override
    public void close() {
        candidateIDQueue.clear();
        resolvedObjects.clear();
        super.close();
        
        OperationStatistics.getOpStatisticCounter("ObjectsAccessedAndRefined").set(candSizeProcessed.get());
        OperationStatistics.getOpStatisticCounter("ReverseIDTransformTime").set(resolutionTime);
        OperationStatistics.getOpStatisticMinMaxCounter("QueryRadius").addValue(minHits);
        if (! originalOperation.isFinished()) {
            originalOperation.endOperation();
        }
    }

    @Override
    public boolean isFinished() {
        return (refinementToEnd.get() <= 0 && refinementToStart.get() <= 0 );
    }
    
}

