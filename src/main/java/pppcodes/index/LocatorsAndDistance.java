/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.index;

import java.nio.BufferUnderflowException;
import mindex.distance.PartialQueryPPPDistanceCalculator;
import mindex.distance.QueryPPPDistanceCalculator;

/**
 * 
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class LocatorsAndDistance implements RankableLocators {
    
    /** Query-object distance or any other float value to sort the objects */
    private final float distance;
    
    /** ID(s) corresponding to given PPP */
    private int [] locators;

    /** Buffer to read the locators */
    private byte [] locatorBuffer;

    /** Buffer position to read the locators */
    private int locatorBufferPosition;

    /** Read-writer */
    private PPPCodeReadWriter locatorReader;
    
    
    // ***************** Constructors *****************//

    /**
     * Reads and create a PPP code object from specified byte buffer creating also the single PPP-Code distance using
     *  given distance calculator.
     * @param input byte buffer to read the data from (with internal position)
     * @param pppCodeReader an object that can read the data from the binary representation
     * @param tailArrayLengthToRead length of the PPP array
     * @param calculator encapsulation of a query object that can calculate single distance from the created PPPCode object
     * @param pppUpper prefix of the single PPP code to be used for creating the distance
     * @throws BufferUnderflowException if the buffer does not have sufficient data to read the 
     */
    public LocatorsAndDistance(ByteBufferBits input, PPPCodeReadWriter pppCodeReader, int tailArrayLengthToRead, 
            QueryPPPDistanceCalculator calculator, short [] pppUpper) throws BufferUnderflowException {
        short[] ppp = pppCodeReader.readPPP(input, tailArrayLengthToRead, tailArrayLengthToRead + pppUpper.length);
        System.arraycopy(pppUpper, 0, ppp, 0, pppUpper.length);
        this.distance = calculator.getQueryDistance(ppp);
        initLocators((PackageByteBufferBits) input, pppCodeReader);
    }
    
    public LocatorsAndDistance(ByteBufferBits input, PPPCodeReadWriter pppCodeReader, int tailArrayLengthToRead, 
            PartialQueryPPPDistanceCalculator particalCalculator, int levelToStartFrom, float topDistance) throws BufferUnderflowException {
        short[] ppp = pppCodeReader.readPPP(input, tailArrayLengthToRead, tailArrayLengthToRead);
        this.distance = topDistance + particalCalculator.getPartialQueryDistance(ppp, levelToStartFrom);
        initLocators((PackageByteBufferBits) input, pppCodeReader);
    }

    public LocatorsAndDistance(float distance, int[] locators) {
        this.distance = distance;
        this.locators = locators;
    }
    
    private void initLocators(PackageByteBufferBits input, PPPCodeReadWriter pppCodeReader) throws BufferUnderflowException {
        this.locatorReader = pppCodeReader;
        this.locatorBuffer = ((PackageByteBufferBits) input).getBuffer();
        this.locatorBufferPosition = input.positionInBits();
        try {
            pppCodeReader.skipIDs(input);
        } catch (ArrayIndexOutOfBoundsException ex) {
            throw new BufferUnderflowException();
        }
    }
    
    
    @Override
    public int [] getLocators() {
        readLocators();
        return locators;
    }

    @Override
    public final RankableLocators readLocators() {
        if (locators == null) {
            this.locators = locatorReader.readIDArray(locatorReader.createNewBuffer(locatorBufferPosition, locatorBuffer));
        }
        return this;
    }
    
    
    // *******************    Comparable     *********************** //
    
    @Override
    public float getQueryDistance() {
        return distance;
    }
}
