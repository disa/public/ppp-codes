/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.index;

/**
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public interface ByteBufferBits {

    /**
     * Reads an integer from this buffer stored at the specified precision (in bits).
     * @param bits number of bits occupied by the integer to read and returned
     * @return the read integer
     * @throws ArrayIndexOutOfBoundsException if the buffer does not contain specified number of bits
     */
    public int get(int bits) throws ArrayIndexOutOfBoundsException;

    /**
     * Stores an integer to this buffer with the specified precision (in bits).
     * @param value the integer value to be stored
     * @param bits number of bits to be occupied by the integer 
     * @throws ArrayIndexOutOfBoundsException if the buffer does have space for the specified number of bits
     */
    public void put(int value, int bits) throws ArrayIndexOutOfBoundsException;

    /**
     * Skips a given number of bits in this buffer (sets the position to current position plus the passed
     *  number of bits.
     * @param bits number of bits to be skipped
     * @throws ArrayIndexOutOfBoundsException if the buffer does have space to skip specified number of bits and stay within
     */
    public void skip(int bits) throws ArrayIndexOutOfBoundsException;
    
    /**
     * Returns the current position in bytes (bits rounded up)
     * @return the current position in bytes (bits rounded up)
     */
    public int position();

    /**
     * Returns the current position in bits
     * @return current position in bits
     */
    public int positionInBits();
    
    /**
     * Sets the new position of this buffer (in bits)
     * @param newPositionBits  new position in bits
     */
    public void positionInBits(int newPositionBits);
    
    /**
     * Returns the size (limit) of this buffer (in bytes)
     * @return the size (limit) of this buffer (in bytes)
     */
    public int limit();

    /**
     * Returns the size (limit) of this buffer (in bits)
     * @return the size (limit) of this buffer (in bits)
     */
    public int limitInBits();
        
    /**
     * Sets new size (limit) of this buffer (in bits)
     * @param newLimitBits the new size (limit) of this buffer (in bits)
     */
    public void limitInBits(int newLimitBits);
    
}
