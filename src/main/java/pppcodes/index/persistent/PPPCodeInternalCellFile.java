/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.index.persistent;


import messif.algorithms.AlgorithmMethodException;
import messif.buckets.BucketStorageException;
import mindex.MetricIndex;
import mindex.navigation.VoronoiCell;
import mindex.navigation.VoronoiInternalCell;
import pppcodes.index.PPPCodeInternalCell;

/**
 * An internal PPP-Code cell that creates leaves which do not store the actual data by serialization 
 * but in a separate binary storage {@link PPPCodeLeafCellFile}.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class PPPCodeInternalCellFile extends PPPCodeInternalCell {

    /** Class id for serialization. */
    private static final long serialVersionUID = 412501L;
    
    /**
     * Create new object initializing cluster number, level and parentNode. The created node is empty - no child nodes are created!
     * @param mIndex M-Index logic
     * @param parentNode parent node of this node
     * @param pivotCombination the pivot combination corresponding to this cluster, e.g. [3,2,4] for cluster C_{3,2,4}
     * @throws AlgorithmMethodException if the cluster storage was not created successfully
     */
    public PPPCodeInternalCellFile(MetricIndex mIndex, VoronoiInternalCell parentNode, short[] pivotCombination) throws AlgorithmMethodException {
        super(mIndex, parentNode, pivotCombination);
    }
    
    /**
     * Creates a specific child according to the type of this internal cell (data/nodata etc.)
     * @param internalNode if to create an internal or a leaf cell
     * @return newly created cell
     * @throws messif.algorithms.AlgorithmMethodException if the creation failed
     */
    @Override
    public VoronoiCell createSpecificChildCell(boolean internalNode, short[] newCombination) throws AlgorithmMethodException {
        return internalNode ? new PPPCodeInternalCellFile(mIndex, this, newCombination) :
                new PPPCodeLeafCellFile(mIndex, this);        
    }
    
    /**
     * Forces write the data from memory into the PPP-Code file storage. This method is to be called
     *  e.g. after the storage is set for the fist time.
     * @throws BucketStorageException if the reading fails
     */
    public void writeToStorage() throws BucketStorageException {
        for (VoronoiCell child : children) {
            if (child != null) {
                if (child instanceof PPPCodeInternalCellFile) {
                    ((PPPCodeInternalCellFile) child).writeToStorage();
                } else if (child instanceof PPPCodeLeafCellFile) {
                    ((PPPCodeLeafCellFile) child).writeToStorage();
                } else {
                    throw new IllegalStateException("this node is not instance of the 'File' branch: "  +child);
                }
            }
        }
    }    
    
    /**
     * This is just a testing method that should not be called under normal circumstances.
     * Reads all the data from PPP-Code file storage into the memory.
     * @throws BucketStorageException if the reading fails
     */
    public void reloadDataTest() throws BucketStorageException {
        for (VoronoiCell child : children) {
            if (child != null) {
                if (child instanceof PPPCodeInternalCellFile) {
                    ((PPPCodeInternalCellFile) child).reloadDataTest();
                } else if (child instanceof PPPCodeLeafCellFile) {
                    ((PPPCodeLeafCellFile) child).reloadDataTest();
                } else {
                    throw new IllegalStateException("this node is not instance of the 'File' branch: "  +child);
                }
            }
        }
    }
}
