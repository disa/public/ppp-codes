/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.algorithms;

import java.util.List;
import java.util.Properties;
import messif.algorithms.Algorithm;
import messif.algorithms.AlgorithmMethodException;
import messif.operations.AbstractOperation;
import mindex.algorithms.MIndexAlgorithm;
import pppcodes.PPPCodeIndex;

/**
 * Encapsulation of a single PPP-Code Voronoi tree (its configuration and an algorithm).
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class PPPCodeSingleAlgorithm extends MIndexAlgorithm {

    /** Class serial id for serialization. */
    private static final long serialVersionUID = 203001L;    
    
    /**
     * Creates the M-Index algorithm given the file name file name containing M-Index properties for new M-Index.
     *
     * @param properties text file with M-Index properties settings
     * @param prefix prefix (e.g. "mindex.") for items in property file that should be considered (the rest is ignored)
     * @throws java.lang.InstantiationException if the file does not contain what it should contain
     * @throws messif.algorithms.AlgorithmMethodException if anything else goes wrong
     */
    @Algorithm.AlgorithmConstructor(description = "Constructor from a created properties object", arguments = {"M-Index properties", "prefix"})
    public PPPCodeSingleAlgorithm(Properties properties, String prefix) throws InstantiationException, AlgorithmMethodException {
        this("PPPCode Single Algorithm", properties, prefix);
    }

    /**
     * Creates the M-Index algorithm given the file name file name containing M-Index properties for new M-Index.
     *
     * @param name algorithm name 
     * @param properties text file with M-Index properties settings
     * @param prefix prefix (e.g. "mindex.") for items in property file that should be considered (the rest is ignored)
     * @throws java.lang.InstantiationException if the file does not contain what it should contain
     * @throws messif.algorithms.AlgorithmMethodException if anything else goes wrong
     */
    @Algorithm.AlgorithmConstructor(description = "Constructor from a created properties object", arguments = {"Algorithm name", "M-Index properties", "prefix"})
    public PPPCodeSingleAlgorithm(String name, Properties properties, String prefix) throws InstantiationException, AlgorithmMethodException {
        super(name, new PPPCodeIndex(properties, prefix));
    }

    /**
     * Internal constructor that creates the algorithm given its name and PPP-Code config object.
     * @param name algorithm name
     * @param index PPP-Code configuration object
     * @throws java.lang.InstantiationException if the file does not contain what it should contain
     * @throws messif.algorithms.AlgorithmMethodException if anything else goes wrong
     */
    protected PPPCodeSingleAlgorithm(String name, PPPCodeIndex index) throws InstantiationException, AlgorithmMethodException {
        super(name, index);
    }
    
    // ************************      Getters and setters      ************************* //

    /** 
     * Getter for internal use within the PPP-Codes.
     * @return the object with a single PPP-Tree configuration
     */
    @Override
    public PPPCodeIndex getmIndex() {
        return (PPPCodeIndex) mIndex;
    }
    
    @Override
    public List<Class<? extends AbstractOperation>> getSupportedOperations() {
        return mIndex.getSupportedOpList();
    }    
    
}
