/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.algorithms;

import java.util.Properties;
import messif.algorithms.Algorithm;
import messif.algorithms.AlgorithmMethodException;
import messif.buckets.BucketStorageException;
import messif.buckets.storage.impl.DiskStorage;
import pppcodes.PPPCodeIndexFile;
import pppcodes.index.persistent.PPPCodeLeafCellFile;

/**
 * Encapsulation of a single PPP-Code Voronoi tree (its configuration and an algorithm) where 
 *  the Voronoi leaves are stored using {@link PPPCodeLeafCellFile} in an external storage, which
 *  is updated every time an object is inserted or deleted.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class PPPCodeSingleAlgorithmFile extends PPPCodeSingleAlgorithm {

    /** Class serial id for serialization. */
    private static final long serialVersionUID = 205001L;    
    
    /**
     * Creates the M-Index algorithm given the file name file name containing M-Index properties for new M-Index.
     *
     * @param properties text file with M-Index properties settings
     * @param prefix prefix (e.g. "mindex.") for items in property file that should be considered (the rest is ignored)
     * @throws java.lang.InstantiationException if the file does not contain what it should contain
     * @throws messif.algorithms.AlgorithmMethodException if anything else goes wrong
     */
    @Algorithm.AlgorithmConstructor(description = "Constructor from a created properties object", arguments = {"M-Index properties", "prefix"})
    public PPPCodeSingleAlgorithmFile(Properties properties, String prefix) throws InstantiationException, AlgorithmMethodException {
        this("PPPCode Single Algorithm with leaves in file", properties, prefix);
    }

    /**
     * Creates the M-Index algorithm given the file name file name containing M-Index properties for new M-Index.
     *
     * @param name algorithm name 
     * @param properties text file with M-Index properties settings
     * @param prefix prefix (e.g. "mindex.") for items in property file that should be considered (the rest is ignored)
     * @throws java.lang.InstantiationException if the file does not contain what it should contain
     * @throws messif.algorithms.AlgorithmMethodException if anything else goes wrong
     */
    @Algorithm.AlgorithmConstructor(description = "Constructor from a created properties object", arguments = {"Algorithm name", "M-Index properties", "prefix"})
    public PPPCodeSingleAlgorithmFile(String name, Properties properties, String prefix) throws InstantiationException, AlgorithmMethodException {
        super(name, new PPPCodeIndexFile(properties, prefix));
    }

    // ************************      Getters and setters      ************************* //

    /** 
     * Getter for internal use within the PPP-Codes.
     * @return the object with a single PPP-Tree configuration
     */
    @Override
    public PPPCodeIndexFile getmIndex() {
        return (PPPCodeIndexFile) mIndex;
    }
    
    /**
     * Sets the disk storage for the Voronoi tree leaves to this PPP-Codes algorithm.
     * @param leafStorage disk storage to manage the Voronoi tree leaves
     */
    public void setLeafStorage(DiskStorage<PPPCodeLeafCellFile.PPPCodeLeafData> leafStorage) throws BucketStorageException {
        getmIndex().setLeafStorage(leafStorage);
    }    
    
}
