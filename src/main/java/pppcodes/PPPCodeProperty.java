/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes;

import mindex.Parameter;
import mindex.Parameter.Type;
import mindex.ParameterImpl;
import mindex.ParameterImpl.ParamClass;

/**
 * Enumeration of all PPP-Code parameters.
 *  It stores <ul>
 *  <li>property key</li>
 *  <li>value type</li>
 *  <li>default value</li>
 *  <li>comment text</li>
 *  <li>flag saying if the a this param should be used via a variable to be passed from the messif-start script</li>
 * </ul>
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
 public enum PPPCodeProperty implements Parameter {

     ID_OBJECT_STORE ("createDiskIndex", Type.BOOL, "false", "# if true, a disk storage with ID-object index is created and used for candidate refinement"),
     
     LOCATOR_CONVERTER ("locatorConverter", Type.OBJECT, "null", "# contains either null of an instance of the string-integer locator converter")
     
     ;
    
     
     // ***********************               Fields            ********************** //
     
     private final ParameterImpl parameter;
     
     @Override
     public String getKey() {
         return parameter.getKey();
     }

     @Override
     public Type getType() {
         return parameter.getType();
     }

     @Override
     public String getDefaultValue() {
         return parameter.getDefaultValue();
     }

     @Override
     public String getComment() {
         return parameter.getComment();
     }
     
     /**
      * Internal private constructor setting all fields.
      * 
      * @param key property key of this parameter
      * @param type type of the parameter
      * @param defaultValue default string
      * @param comment comment string (can be empty)
      */
     private PPPCodeProperty(String propertyString, Type type, String defaultValue, String comment) {
         parameter = new ParameterImpl(ParamClass.OTHER, propertyString, type, defaultValue, comment, false);
     }          
     
}
