/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.ids;

import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * An string-integer locator convertor that keeps two in-memory hash tables of string-ids for both-ways conversions.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class HashtableStringIntConvertor implements LocatorStringIntConvertor {

    /** Class serial id for serialization. */
    private static final long serialVersionUID = 8602401L;

    /** No value special int value. */
    public static final int NO_VALUE_INT = Integer.MIN_VALUE;
    
    /** If a locator string is mapped to this value, it means that it should be  */
    public static final int TEMPORARY_INT_VALUE = 0;
    
    /** The first generated integer object ID = 1 */
    public static final int INITIAL_GENERATED_INT = 1;
    
    /** Counter for the object ID generation */
    protected final AtomicInteger nextGeneratedInt;
    
    /** String-integer hash map. */
    protected final TObjectIntMap<String> strIntMap = new TObjectIntHashMap<>(100, 0.7f, NO_VALUE_INT);

    /** Id-string hash map. */
    protected final TIntObjectMap<String> intStrMap = new TIntObjectHashMap<>(100, 0.7f);

    /**
     * Creates a new hast-table string-int convertor with initial value {@link #INITIAL_GENERATED_INT}.
     */
    public HashtableStringIntConvertor() {
        this(new AtomicInteger(INITIAL_GENERATED_INT));
    }    

    /**
     * Creates a new hast-table string-int convertor with specified first value that must be greater than 0.
     * @param nextGeneratedInt first value that must be greater than 0
     */
    public HashtableStringIntConvertor(AtomicInteger nextGeneratedInt) {
        if (nextGeneratedInt.intValue() <= 0) {
            throw new IllegalArgumentException("the first generated ID must be greater than 0: " + nextGeneratedInt.intValue());
        }
        this.nextGeneratedInt = nextGeneratedInt;
    }    
    
    // *******************     Implementation of the string -> int locator converter    ******************* //

    @Override
    public int getIntLocator(String locator) throws IllegalArgumentException {
        int retval = strIntMap.putIfAbsent(locator, TEMPORARY_INT_VALUE);
        if ((retval == NO_VALUE_INT) || (retval == TEMPORARY_INT_VALUE)) {
            synchronized (this) {
                if ((retval = strIntMap.putIfAbsent(locator, TEMPORARY_INT_VALUE)) != TEMPORARY_INT_VALUE) {
                    return retval;
                }
                retval = nextGeneratedInt.getAndIncrement();
                strIntMap.put(locator, retval);
                intStrMap.put(retval, locator);
            }
        }
        return retval;
    }

    @Override
    public String getStringLocator(int id) {
        return intStrMap.get(id);
    }    
}
