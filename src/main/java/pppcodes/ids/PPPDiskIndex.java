/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.ids;

import messif.buckets.index.impl.DiskStorageMemoryIntIndex;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;
import messif.buckets.BucketStorageException;
import messif.buckets.index.Search;
import messif.buckets.storage.impl.DiskStorage;
import messif.objects.LocalAbstractObject;
import messif.objects.PrecomputedDistancesFixedArrayFilter;
import messif.objects.keys.IntegerKey;
import messif.utility.Convert;
import mindex.MIndexObjectInitilizer;
import mindex.MIndexProperties;
import mindex.MetricIndex;

/**
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class PPPDiskIndex implements IDObjectRAStorage<LocalAbstractObject>, ObjectLocatorConvertor {
        
    /** The object storage */
    private final DiskStorageMemoryIntIndex intObjectIndex;
    
    /** Directory into which this index should be serialized */
    private final File serializationDir;

    /** The set of pivots for precomputing distances (can be null) */
    protected LocalAbstractObject[] pivots;

    /** *  The first generated integer object ID (for {@link ObjectLocatorConvertor} interface implementation). */
    public static final int INITIAL_GENERATED_INT = 0;
    
    /** *  Counter for the object ID generation (for {@link ObjectLocatorConvertor} interface implementation). */
    protected final AtomicInteger nextGeneratedInt;
    
    
    /** Creates the PPP disk index given a disk storage. */
    protected PPPDiskIndex(DiskStorage<LocalAbstractObject> storage, File serializationDir, LocalAbstractObject[] pivots) {
        this.intObjectIndex = new DiskStorageMemoryIntIndex(storage);
        this.serializationDir = serializationDir;
        this.pivots = pivots;
        this.nextGeneratedInt = new AtomicInteger(INITIAL_GENERATED_INT);
    }

    /** Creates the PPP disk index from an existing index and having ID of the next inserted object. */
    protected PPPDiskIndex(DiskStorageMemoryIntIndex intObjectIndex, File serializationDir, LocalAbstractObject[] pivots, AtomicInteger nextID) {
        this.intObjectIndex = intObjectIndex;
        this.serializationDir = serializationDir;
        this.pivots = pivots;
        this.nextGeneratedInt = nextID;
    }

    
    // ****************     Static creators and storage   ******************* //

    /** Default name of the serialized PPPDisk index */
    public static final String SERIALIZATION_FILE = "index.bin";
    
    /** Default name of the storage that is expected to be stored in the same directory as the serialized index */
    public static final String STORAGE_FILE = "storage.bin";
    
    /** Default properties prefix for configuration of the {@link #intObjectIndex}. */
    public static final String CONFIG_PREFIX = "storage.";

    /** Default properties prefix for filtering pivots configuration. */
    public static final String PIVOTS_PREFIX = "pivots.";
    
    public static PPPDiskIndex restoreFromDir(File dir, LocalAbstractObject[] pivots) throws IOException {
        File file = new File(dir, SERIALIZATION_FILE);
        try (ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)))) {
            DiskStorageMemoryIntIndex index = (DiskStorageMemoryIntIndex) in.readObject();
            AtomicInteger nextID = new AtomicInteger(index.getMaxKey() + 1);
            return new PPPDiskIndex(index, dir, pivots, nextID);
        } catch (NullPointerException |ClassNotFoundException | ClassCastException ex) {
            throw new IOException(ex);
        }
    }

    public static PPPDiskIndex createInDir(Properties configuration) throws IOException {
        Map<String, Object> storageProperties = MetricIndex.fillBucketProperties(CONFIG_PREFIX, configuration);        
        File dir = Convert.getParameterValue(storageProperties, "dir", File.class, new File(System.getProperty("user.dir")));
        storageProperties.put("dir", dir);
        File storageFile = Convert.getParameterValue(storageProperties, "file", File.class, new File(dir, STORAGE_FILE));
        storageProperties.put("file", storageFile);

        // create pivots to calculate the precoputed distances
        LocalAbstractObject [] pivots = MIndexObjectInitilizer.initPivots(new MIndexProperties(configuration, PIVOTS_PREFIX));
        
        // if the specified directory contains serialized file and data storage then restore it
        if (storageFile.exists() && new File(dir, SERIALIZATION_FILE).exists()) {
            return restoreFromDir(dir, pivots);
        }
        try {
            return new PPPDiskIndex(DiskStorage.create(LocalAbstractObject.class, storageProperties), dir, pivots);
        } catch (InstantiationException ex) {
            throw new IOException(ex);
        }
    }
        
    public void storeToFile() throws IOException {
        File file = new File(serializationDir, SERIALIZATION_FILE);
        try (ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)))) {
            out.writeObject(intObjectIndex);
        }
    }

    
    // ************************    Methods from the IDObjectRAStorage     **************************** //
    
    @Override
    public LocalAbstractObject readObject(int id) {
        Iterator<LocalAbstractObject> objects = intObjectIndex.getObjects(Collections.singletonList(id));
        if (objects.hasNext()) {
            return objects.next();
        }
        return null;
    }

    @Override
    public Iterator<LocalAbstractObject> readObjects(Collection<Integer> ids) {
        return intObjectIndex.getObjects(ids);
    }    
    
    @Override
    public boolean storeObject(LocalAbstractObject object) throws BucketStorageException {
        return intObjectIndex.add(object);
    }

    @Override
    public Search<LocalAbstractObject> getAllObjects() {
        return intObjectIndex.getAllObjects();
    }    

    @Override
    public LocalAbstractObject preprocessObject(LocalAbstractObject object) {
        if (pivots != null) {
            float[] distances = new float[pivots.length];
            for (int distanceIndex = 0; distanceIndex < pivots.length; distanceIndex++) {
                distances[distanceIndex] = object.getDistance(pivots[distanceIndex]);
            }
            PrecomputedDistancesFixedArrayFilter filter = new PrecomputedDistancesFixedArrayFilter(object);
            filter.setFixedPivotsPrecompDist(distances);
        }
        return object;
    }        
    

    // *******************     Implementation of the string -> int locator converter    ******************* //

    @Override
    public int getAndStoreIntLocator(LocalAbstractObject object) {
        if (object.getObjectKey() instanceof IntegerKey) {
            return ((IntegerKey) object.getObjectKey()).key;
        }
        int retVal = nextGeneratedInt.getAndIncrement();
        object.setObjectKey(new IntegerKey(object.getLocatorURI(), retVal));
        return retVal;
    }

    @Override
    public int getIntLocator(String locator) {
        return nextGeneratedInt.getAndIncrement();
    }
    
    @Override
    public String getStringLocator(int id) {
        LocalAbstractObject object = readObject(id);
        return (object == null) ? null : object.getLocatorURI();
    }

    @Override
    public String[] getStringLocators(List<Integer> ids) {
        Iterator<LocalAbstractObject> readObjects = readObjects(ids);
        String [] retVal = new String [ids.size()];
        while (readObjects.hasNext()) {
            LocalAbstractObject next = readObjects.next();
            int integerID = DiskStorageMemoryIntIndex.getIntegerID(next);
            for (int i = 0; i < ids.size(); i++) {
                if (ids.get(i) == integerID) {
                    retVal[i] = next.getLocatorURI();
                    break;
                }
            }
        }
        return retVal;
    }
    
}
